import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';


class FormZodiac extends React.Component{

  render(){
    return (
      <div className="container">
        <h2>Form Zodiac</h2>
          <div className="content">
            <div className="row">
              <label>Name</label> <input type="text" id="name" placeholder="Input your name"></input>
            </div>
            <div className="row">
              <label>Birthdate</label> <input type="date" id="birthDate" placeholder="Your Birth Date"></input>
            </div>
            <div className="row">
              <label>Zodiac</label> <input type="text" id="zodiac"  readOnly></input><button className="generate" onClick={generatedZodiac} >Generate</button>
            </div>
          </div>
          <button className="submit" onClick={send}>Send</button>
      </div>
    )
  }
}

function send(){
  let post = {
    method : 'POST',
    headers : {
      'Content-Type' : 'application/json; charset=UTF-8'
    },
    body : JSON.stringify({
      name : document.getElementById("name").value,
      birthDate : document.getElementById("birthDate").value,
      zodiac : document.getElementById("zodiac").value
    })
  }
  let send = fetch('http://localhost:8080/post', post)
} 

function generatedZodiac() {
  let birthDate = document.getElementById("birthDate").value
  let date = birthDate.substring(8,10)
  let month = birthDate.substring(5,7)

  if(date != null && month != null){
    switch(parseInt(month)){
      case 1 : if (parseInt(date) <= 21){
                  document.getElementById("zodiac").placeholder="Capricorn";
                  document.getElementById("zodiac").value="Capricorn";
                } else {
                  document.getElementById("zodiac").placeholder="Aquarius";
                  document.getElementById("zodiac").value="Aquarius";
                } break;
      case 2 : if (parseInt(date) <= 19){
                  document.getElementById("zodiac").placeholder="Aquarius";
                  document.getElementById("zodiac").value="Aquarius";
                } else {
                  document.getElementById("zodiac").placeholder="Pisces";
                  document.getElementById("zodiac").value="Pisces";
                } break;
      case 3 : if (parseInt(date) <= 20){
                  document.getElementById("zodiac").placeholder="Pisces";
                  document.getElementById("zodiac").value="Pisces";
                } else {
                  document.getElementById("zodiac").placeholder="Aries";
                  document.getElementById("zodiac").value="Aries";
                } break;
      case 4 : if (parseInt(date) <= 19){
                  document.getElementById("zodiac").placeholder="Aries";
                  document.getElementById("zodiac").value="Aries";
                } else {
                  document.getElementById("zodiac").placeholder="Taurus";
                  document.getElementById("zodiac").value="Taurus";
                } break;
      case 5 : if (parseInt(date) <= 20){
                  document.getElementById("zodiac").placeholder="Taurus";
                  document.getElementById("zodiac").value="Taurus";
                } else {
                  document.getElementById("zodiac").placeholder="Gemini";
                  document.getElementById("zodiac").value="Gemini";
                } break;
      case 6 : if (parseInt(date) <= 21){
                  document.getElementById("zodiac").placeholder="Gemini";
                  document.getElementById("zodiac").value="Gemini";
                } else {
                  document.getElementById("zodiac").placeholder="Cancer";
                  document.getElementById("zodiac").value="Cancer";
                } break;
      case 7 : if (parseInt(date) <= 23){
                  document.getElementById("zodiac").placeholder="Cancer";
                  document.getElementById("zodiac").value="Cancer";
                } else {
                  document.getElementById("zodiac").placeholder="Leo";
                  document.getElementById("zodiac").value="Leo";
                } break;
      case 8 : if (parseInt(date) <= 23){
                  document.getElementById("zodiac").placeholder="Leo";
                  document.getElementById("zodiac").value="Leo";
                } else {
                  document.getElementById("zodiac").placeholder="Virgo";
                  document.getElementById("zodiac").value="Virgo";
                } break;
      case 9 : if (parseInt(date) <= 22){
                  document.getElementById("zodiac").placeholder="Virgo";
                  document.getElementById("zodiac").value="Virgo";
                } else {
                  document.getElementById("zodiac").placeholder="Libra";
                  document.getElementById("zodiac").valuer="Libra";
                } break;
      case 10 : if (parseInt(date) <= 23){
                  document.getElementById("zodiac").placeholder="Libra";
                  document.getElementById("zodiac").value="Libra";
                } else {
                  document.getElementById("zodiac").placeholder="Scorpio";
                  document.getElementById("zodiac").value="Scorpio";
                } break;
      case 11 : if (parseInt(date) <= 22){
                  document.getElementById("zodiac").placeholder="Scorpio";
                  document.getElementById("zodiac").value="Scorpio";
                } else {
                  document.getElementById("zodiac").placeholder="Sagitarius";
                  document.getElementById("zodiac").value="Sagitarius";
                } break;
      case 11 : if (parseInt(date) <= 22){
                  document.getElementById("zodiac").placeholder="Sagitarius";
                  document.getElementById("zodiac").value="Sagitarius";
                } else {
                  document.getElementById("zodiac").placeholder="Capricorn";
                  document.getElementById("zodiac").value="Capricorn";
                } break;
      default : alert("Please insert your birth date!")
    }
  }
}

ReactDOM.render(
  <React.StrictMode>
    <FormZodiac />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

package com.formative21.formative21.Repository;

import com.formative21.formative21.POJO.Zodiac;

import org.springframework.data.repository.CrudRepository;

public interface ZodiacRepo extends CrudRepository<Zodiac, Integer> {
    Zodiac save(Zodiac zodiac);
}

package com.formative21.formative21.MainController;

import com.formative21.formative21.POJO.Zodiac;
import com.formative21.formative21.Repository.ZodiacRepo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class MainController {
    @Autowired
    ZodiacRepo zodiacRepo;


    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(value = "/post", consumes = "application/json")
    public String addRepo(@RequestBody Zodiac zodiac) {
        zodiacRepo.save(zodiac);
        return "Success";
    }
    
}
